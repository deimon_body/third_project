import {appendRoomElement} from './views/room.mjs';
import {showInputModal,showMessageModal} from './views/modal.mjs'
const username = sessionStorage.getItem('username');

if (!username) {
	window.location.replace('/login');
}

const socket = io('', { query: { username } });

const createRoomBtn = document.getElementById("add-room-btn");

function joinUser(){
	//countOfUser+1
	//socket.emit("appendRoom",UpdateRoomInfo);
	
}

createRoomBtn.addEventListener('click',()=>{
	
	
	let roomName;
	let newRoom;
	let createRoomName = val=>{
		roomName = val;
	};

	
	const createNewRoom = ()=>{
		const newRoomInfo = {
			name:roomName,
			numberOfUsers:0,
			onJoin:()=>{
				console.log("joined!")
			}
		}
		newRoom = appendRoomElement(newRoomInfo) 
		socket.emit("appendRoom",newRoomInfo);
	}

	showInputModal({
		title:"Create New Room",
		onChange:createRoomName,
		onSubmit:createNewRoom
	})	
})


const updateRoomsWrapper = (newRoom)=>{
	appendRoomElement(newRoom)
}

const updateRoom = ()=>{

}
// const redirectUser = ()=>{
	
// 	window.location.replace('/login');
// }
// const notCurrentUser = ()=>{
// 	sessionStorage.removeItem('username')
// 	showMessageModal({message:'User name was created,change your name',onClose:redirectUser})
// }
// socket.on('notCurrentUser',notCurrentUser)
socket.on('UPDATE_ROOM_WRAPPER',updateRoomsWrapper);